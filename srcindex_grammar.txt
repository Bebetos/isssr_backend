	% questa flag è richiesta dal programma per identificare la produzione con l'assioma
	__AXIOM__ = ⛸

	% il simbolo '⛸ ' è l'assioma della grammatica, ed ammette un'espressione di base come 
	% simbolo di partenza, tale espressione di base può essere preceduta dal segno meno.
	⛸ -> ⛽

	% il simbolo '⛽' ammette chiusura tra parentesi di ampiezza arbitraria.
	⛽ -> (⛽) 

	% il simbolo '⛽' ammette combinazioni con tutti gli operatori matematici di base.
	⛽ -> ⛽*⛽ | ⛽+⛽ | ⛽-⛽ | ⛽/⛽
	
	% il simbolo '⛽' espande in un numero intero, un numero reale o una funzione.
	⛽ -> ♻ | ♻.♻ | ⛿

	% il simbolo '♻' espande in un numero intero arbitrariamente grande.
	♻ -> ⛔♻ | ⛔
	⛔ -> 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9

	% il simbolo '⛿' espande nelle funzioni riconosciute.
	⛿ -> product.GiacenzaMedia
	⛿ -> product.Deperito
	⛿ -> product.QuantitaDisponibile
	⛿ -> product.Prezzo
	⛿ -> product.Costo
	⛿ -> product.QuantitaOrdinata
