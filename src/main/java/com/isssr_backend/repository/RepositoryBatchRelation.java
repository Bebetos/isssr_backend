package com.isssr_backend.repository;

import com.isssr_backend.entity.product.BatchesRelation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

/**
 * Created by andre on 24/06/2017.
 */
@Repository
public interface RepositoryBatchRelation extends MongoRepository<BatchesRelation, String> {

    ArrayList<BatchesRelation> findAll();
}
