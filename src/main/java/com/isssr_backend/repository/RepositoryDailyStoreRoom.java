package com.isssr_backend.repository;

import com.isssr_backend.entity.food.DailyStoreRoom;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by andre on 05/07/2017.
 */
@Repository
public interface RepositoryDailyStoreRoom extends MongoRepository<DailyStoreRoom, String> {
    DailyStoreRoom findDailyStoreRoomByDateIsAndLocation(Date date, String location);

    ArrayList<DailyStoreRoom> findDailyStoreRoomByDateBetweenAndLocation(Date start, Date end, String location);
}
