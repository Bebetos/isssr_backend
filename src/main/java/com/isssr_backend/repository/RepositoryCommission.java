package com.isssr_backend.repository;

import com.isssr_backend.entity.product.Commission;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

/**
 * Created by GM on 24/06/2017.
 */
@Repository
public interface RepositoryCommission extends MongoRepository<Commission, String> {
    ArrayList<Commission> findAll();
}
