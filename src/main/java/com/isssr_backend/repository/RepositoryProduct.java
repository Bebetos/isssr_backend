package com.isssr_backend.repository;

import com.isssr_backend.entity.product.Product;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alberto on 31/05/2017.
 */
@Repository
public interface RepositoryProduct extends MongoRepository<Product, String> {


    ArrayList<Product> findAll();

    Product findProductByDescriptionEquals(String description);

    /**
     * Query che cerca i prodotti in base a una stringa fourchar data dal front end. Il paramtro fourchar, verifica la stessa stringa in UPPERCASE.
     *
     * @param fourchar Parametro trasformato in lowercase
     * @param FOURCHAR Parametro in uppercase
     * @return List<Product> Contenente i prodotti la cui description contiene il fourchar selezionato
     */
    @Query(fields = "{description : 1, _id : 1}")
    List<Product> findProductByDescriptionContainingOrDescriptionContaining(String fourchar, String FOURCHAR);


}