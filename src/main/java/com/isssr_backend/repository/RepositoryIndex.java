package com.isssr_backend.repository;

import com.isssr_backend.entity.Index;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

/**
 * Created by GM on 05/06/2017.
 */
@Repository
public interface RepositoryIndex extends MongoRepository<Index, String> {

    ArrayList<Index> findAll();

    Index findByNameIndex(String name);

    ArrayList<Index> findIndexByCategory(String category);

}