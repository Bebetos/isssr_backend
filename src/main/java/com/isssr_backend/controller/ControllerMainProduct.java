package com.isssr_backend.controller;

import com.isssr_backend.entity.TopThree;
import com.isssr_backend.service.ServiceMainProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by andre on 09/07/2017.
 */
@Controller
@RequestMapping("/main")
public class ControllerMainProduct {

    @Autowired
    private ServiceMainProduct serviceMainProduct;

    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/getTopProduct", method = RequestMethod.GET)
    public @ResponseBody
    TopThree top3indexProduct(@RequestParam String index) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {

        return serviceMainProduct.topThreeIndexProduct(index);
    }


    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/getTopFood", method = RequestMethod.GET)
    public @ResponseBody
    TopThree top3indexFood(@RequestParam String index) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {

        return serviceMainProduct.topThreeIndexFood(index);
    }


    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/deleteCache", method = RequestMethod.DELETE)
    public @ResponseBody
    void deleteCache()  {

    serviceMainProduct.deleteCache();

    }





}
