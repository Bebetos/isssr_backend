package com.isssr_backend.controller.controllerFood;

import com.isssr_backend.entity.food.CategoryFood;
import com.isssr_backend.entity.food.Foods;
import com.isssr_backend.entity.message.MessageNewCategoryFood;
import com.isssr_backend.service.serviceFood.ServiceCategoryFood;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by GM on 04/07/2017.
 */
@Controller
@RequestMapping(path = "/catFood")
public class ControllerCategoryFood {

    @Autowired
    ServiceCategoryFood serviceCategoryFood;


    /**
     * Riempe la select dei FOODS in base al nome passato come parametro. Non ha bisogno di locazione poichè i food nella cateogira appartengono
     * tutti alla stessa location
     *
     * @param nameCategory
     * @return ArrayList<Foods>
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/fillSelectFood", method = RequestMethod.GET)
    public @ResponseBody
    ArrayList<Foods> riempiSelect(@RequestParam String nameCategory) {
        return serviceCategoryFood.riempiSelectFood(nameCategory);
    }


    /**
     * Funzione che ritorna tutte le categorie relative ai foods
     *
     * @return ArrayList<CategoryFood>
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/findAll", method = RequestMethod.GET)
    public @ResponseBody
    ArrayList<CategoryFood> findAll() {
        return serviceCategoryFood.findAll();
    }


    /**
     * Funzione che aggiunge una nuova categoria nel DB
     *
     * @param message
     * @return CategoryFood
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/addNewCat", method = RequestMethod.POST)
    public @ResponseBody
    CategoryFood addNewCat(@RequestBody MessageNewCategoryFood message) {
        CategoryFood ret = serviceCategoryFood.addCustomCategory(message);
        return ret;
    }

    /**
     * Riempie le categorie della pagina degli indici in base all'utente, alla locazione
     * e a un eventuale parametro str, che rappresenta l'input dell'utente
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/fillCatIndex", method = RequestMethod.GET)
    public @ResponseBody
    ArrayList<CategoryFood> riempiSelectCategoriePagIndice(@RequestParam String str, @RequestParam String utente, @RequestParam String location) {
        return serviceCategoryFood.riempiSelectIndexCategory(str, utente, location);
    }


    /**
     * Chiamata POST per aggiungere una nuova categoria custom
     *
     * @param mCategory parametro passato dal frontend nel body
     * @ Category ritorna l'oggetto in JSON a schermo
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/addNewCatFood", method = RequestMethod.POST)
    public @ResponseBody
    CategoryFood addNewCategoryFood(@RequestBody MessageNewCategoryFood mCategory) {
        return serviceCategoryFood.addCustomCategory(mCategory);
    }

    /**
     * Chiamata Delete per rimuovere una categoria del food
     *
     *
     * @ Category ritorna l'oggetto in JSON a schermo
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/removeCatFood", method = RequestMethod.DELETE)
    public @ResponseBody
    void deleteCategoryFood(@RequestParam String name,@RequestParam String utente,@RequestParam String location) {

        serviceCategoryFood.deleteCategoryFood(name,utente,location);
        return;
    }

    /**
     * Chiamata Delete per rimuovere una categoria del food
     *
     *
     * @ Category ritorna l'oggetto in JSON a schermo
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/getFoodByCatLoc", method = RequestMethod.GET)
    public @ResponseBody
    ArrayList<Foods> getFoodByCatLoc(@RequestParam String category, @RequestParam String location) {

        return serviceCategoryFood.getFoodByCatLoc(category,location);

    }


}