package com.isssr_backend.controller.controllerProduct;

import com.isssr_backend.entity.product.BatchesRelation;
import com.isssr_backend.service.serviceProduct.ServiceBatchRelation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@Controller
@RequestMapping(path = "/batchRel")
public class ControllerBatchRelation {

    @Autowired
    ServiceBatchRelation serviceBatchRelation;

    /**
     * Metodo GET che ritorna tutti gli oggetti BatchRelation
     *
     * @return ArrayList<BatchRelation>
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/findAll", method = RequestMethod.GET)
    public @ResponseBody
    ArrayList<BatchesRelation> getAll() {
        return serviceBatchRelation.getAll();
    }

    @RequestMapping(path = "/addBatchRel", method = RequestMethod.POST)
    public @ResponseBody
    BatchesRelation addBatchRel(@RequestBody BatchesRelation batchesRelation) {
        BatchesRelation b = serviceBatchRelation.addBatchRel(batchesRelation);
        return b;
    }
}
