package com.isssr_backend.controller.controllerProduct;

import com.isssr_backend.entity.product.Batch;
import com.isssr_backend.service.serviceProduct.ServiceBatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Controller
@RequestMapping(path = "/batch")
public class ControllerBatch {

    @Autowired
    private ServiceBatch serviceBatch;

    /**
     * Chiamata che restituisce tutti i Batch
     *
     * @return List<Batch>
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/findAll", method = RequestMethod.GET)
    public @ResponseBody
    List<Batch> getAll() {
        return serviceBatch.findAll();
    }

    /**
     * Funzione che aggiunge un Batch
     *
     * @return Batch Object  appena aggiunto
     */
    //@CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/addBatch", method = RequestMethod.POST)
    public @ResponseBody
    Batch addNewBatch(@RequestBody Batch batch) {
        return serviceBatch.addNewBatch(batch);
    }
}

