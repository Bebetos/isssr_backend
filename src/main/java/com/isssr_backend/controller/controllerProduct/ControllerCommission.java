package com.isssr_backend.controller.controllerProduct;

import com.isssr_backend.entity.product.Commission;
import com.isssr_backend.service.serviceProduct.ServiceCommission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;

@Controller
@RequestMapping(path = "/commission")
public class ControllerCommission {

    @Autowired
    ServiceCommission mServiceCommission;

    @RequestMapping(path = "/findAll", method = RequestMethod.GET)
    public @ResponseBody
    ArrayList<Commission> getAll() {
        return mServiceCommission.findAll();
    }


    @RequestMapping(path = "/addCommission", method = RequestMethod.POST)
    public @ResponseBody
    Commission addNewCommission(@RequestBody Commission commission) {
        return mServiceCommission.addNewCommission(commission);

    }
}
