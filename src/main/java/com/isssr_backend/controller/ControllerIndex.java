package com.isssr_backend.controller;

import com.isssr_backend.entity.Index;
import com.isssr_backend.entity.message.MessageIndexCompute;
import com.isssr_backend.jasper.ResponseReport;
import com.isssr_backend.service.ServiceIndex;
import com.isssr_backend.service.ServiceReport;
import com.isssr_backend.variables.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;


@Controller
@RequestMapping(path = "/index")
public class ControllerIndex {

    @Autowired
    ServiceIndex serviceIndex;
    @Autowired
    ServiceReport serviceReport;


    /**
     * GET call that return a List of all Indexes
     *
     * @return ArrayList<Index>
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/getIndexBySemantic", method = RequestMethod.GET)
    public @ResponseBody
    ArrayList<Index> getIndexProduct(@RequestParam String category) {
        return serviceIndex.getIndexBySemantic(category);
    }


    /**
     * GET call that return a List of all Indexes
     *
     * @return ArrayList<Index>
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/findAll", method = RequestMethod.GET)
    public @ResponseBody
    ArrayList<Index> findAll() {
        return serviceIndex.getAll();
    }


    /**
     * POST call to add a new index giving the parameters in body
     *
     * @param index
     * @return Index object to show if added in monitor
     */
    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/addIndex", method = RequestMethod.POST)
    public @ResponseBody
    void addNewIndex(@RequestBody Index index, HttpServletResponse response) {
        serviceIndex.addNewIndex(index,response);
    }


    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(path = "/computeIndexProduct", method = RequestMethod.POST)
    public @ResponseBody
    void computeIndexProduct(MessageIndexCompute indexMessage) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        ArrayList<Result> r = serviceIndex.computeIndex(indexMessage);
    }


    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(value = "/getVariables={value}", method = RequestMethod.GET)
    public @ResponseBody
    ArrayList<String> getVariables(@PathVariable String value) {
        return ServiceIndex.getVariableList(value);
    }


    @CrossOrigin(origins = "http://localhost:8012")
    @RequestMapping(value = "/getReport", method = RequestMethod.POST)
    public @ResponseBody
    ResponseReport getProduct(@RequestBody MessageIndexCompute msg) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException, IOException {
        ArrayList<Result> mResult = serviceIndex.computeIndex(msg);
        return serviceReport.sendReport(mResult, msg);
    }
}