package com.isssr_backend.entity.message;

import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Created by GM on 06/07/2017.
 */
@Component
public class MessageNewCategoryFood {


    @Field("food")
    private ArrayList<String> food;
    @Field("categorieFood")
    private ArrayList<String> categorieFood;
    @Field("name")
    private String name;
    @Field("description")
    private String description;
    @Field("utente")
    private String utente;

    @Field("location")
    private String location;

    public ArrayList<String> getFood() {
        return food;
    }

    public void setFood(ArrayList<String> food) {
        this.food = food;
    }

    public ArrayList<String> getCategorieFood() {
        return categorieFood;
    }

    public void setCategorieFood(ArrayList<String> categorieFood) {
        this.categorieFood = categorieFood;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUtente() {
        return utente;
    }

    public void setUtente(String utente) {
        this.utente = utente;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
