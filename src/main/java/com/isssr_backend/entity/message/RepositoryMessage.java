package com.isssr_backend.entity.message;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;


public interface RepositoryMessage extends MongoRepository<MessageProduct, String> {
    List<MessageProduct> findAll();
}
