package com.isssr_backend.entity;


public class MyDate {

    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;
    private int seconds;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year - 1900;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month - 1;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }
}
