package com.isssr_backend.entity.food;

import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.stereotype.Component;

/**
 * Created by GM on 04/07/2017.
 */
@Component
public class pietanzaAnalyticsDtos {
    @Field("pietanzaMenuDto")
    private PietanzaMenuDto pietanzaMenuDto;
    @Field("venduto")
    private int venduto;
    @Field("preparato")
    private int preparato;
    @Field("categoria")
    private String categoria;

    public PietanzaMenuDto getPietanzaMenuDto() {
        return pietanzaMenuDto;
    }

    public void setPietanzaMenuDto(PietanzaMenuDto pietanzaMenuDto) {
        this.pietanzaMenuDto = pietanzaMenuDto;
    }

    public int getVenduto() {
        return venduto;
    }

    public void setVenduto(int venduto) {
        this.venduto = venduto;
    }

    public int getPreparato() {
        return preparato;
    }

    public void setPreparato(int preparato) {
        this.preparato = preparato;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
}
