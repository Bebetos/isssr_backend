package com.isssr_backend.entity.food;

import com.isssr_backend.entity.product.Product;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.stereotype.Component;

/**
 * Created by andre on 05/07/2017.
 */
@Component
public class ItemStore {

    @Field("Product")
    private Product product;
    @Field("avgCosto")
    private double avgCosto;
    @Field("stock")
    private int stock;

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getAvgCosto() {
        return avgCosto;
    }

    public void setAvgCosto(double avgCosto) {
        this.avgCosto = avgCosto;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
