package com.isssr_backend.entity.food;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by GM on 04/07/2017.
 */
@Component
@Document(collection = "Pietanza")
public class PietanzaDto {
    @Field("nome")
    private String nome;
    @Field("prezzo")
    private Double prezzo;

    @Field("etichette")
    private List<String> etichette;

    @Field("ingredienti")
    private List<IngredienteDto> ingredienti;


    public PietanzaDto() {
    }

    public PietanzaDto(String nome, double prezzo, List<String> etichette, List<IngredienteDto> ingredienti) {
        this.nome = nome;
        this.prezzo = new Double(prezzo);
        this.etichette = etichette;
        this.ingredienti = ingredienti;
    }

    public List<IngredienteDto> getIngredienti() {
        return ingredienti;
    }

    public void setIngredienti(List<IngredienteDto> ingredienti) {
        this.ingredienti = ingredienti;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(Double prezzo) {
        this.prezzo = prezzo;
    }

    public List<String> getEtichette() {
        return etichette;
    }

    public void setEtichette(List<String> etichette) {
        this.etichette = etichette;
    }
}
