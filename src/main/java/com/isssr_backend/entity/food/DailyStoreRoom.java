package com.isssr_backend.entity.food;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by andre on 05/07/2017.
 */

//This entity represent the store of each location every day
@Document(collection = "DailyStoreRoom")
public class DailyStoreRoom {

    @Id
    private String Id;

    @Field("location")
    private String location;

    @Field("date")
    private Date date;

    @Field("itemStores")
    private ArrayList<ItemStore> itemStores;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ArrayList<ItemStore> getItemStores() {
        return itemStores;
    }

    public void setItemStores(ArrayList<ItemStore> itemStores) {
        this.itemStores = itemStores;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
