package com.isssr_backend.entity.food;

import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.stereotype.Component;

/**
 * Created by GM on 04/07/2017.
 */

@Component
public class Ingrediente {

    @Field("nomeProdotto")
    private String nomeProdotto;

    @Field("quantita")
    private int quantita;


    public String getNomeProdotto() {
        return nomeProdotto;
    }

    public void setNomeProdotto(String nomeProdotto) {
        this.nomeProdotto = nomeProdotto;
    }

    public int getQuantita() {
        return quantita;
    }

    public void setQuantita(int quantita) {
        this.quantita = quantita;
    }

}
