package com.isssr_backend.variables;

import com.isssr_backend.entity.food.DailyStoreRoom;
import com.isssr_backend.entity.food.ItemStore;
import com.isssr_backend.entity.message.MessageIndexCompute;
import com.isssr_backend.entity.product.Order;
import com.isssr_backend.entity.product.Product;
import com.isssr_backend.entity.product.ReportProductsMC;
import com.isssr_backend.global.GlobalVariable;
import com.isssr_backend.variables.util.Result;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by andre on 03/07/2017.
 */
public class GiacenzaMedia extends Variable {
    // Il costruttore setta solo la semantica della variabile.
    public GiacenzaMedia() {
        this.semantic = "product";
    }

    @Override
    public void computeFood(MessageIndexCompute msg) {

    }

    @Override
    public void computeEconomic(MessageIndexCompute msg) {

    }

    @Override
    public void computeProduct(MessageIndexCompute msg) {
        //Quantità di 1 Giorno in millisecondi usata per il between
        long giorno = 86400000;
        resultProduct.clear();

        //for che trova di volta in volta il product equivalente alla descrizione mandata nel message
        for (int i = 0; i < msg.getDescProduct().size(); i++) {
            Product prod = servPr.findByDesc(msg.getDescProduct().get(i));

            //System.out.println("MSG LOCATION:" + msg.getLocation());

            /**
             * Blocco di codice per evitare che la between di spring tagli le 2 date limite
             */

            long temp = msg.getStart().getTime() - giorno;
            Date start = new Date();
            start.setTime(temp);
            System.out.println("DATA START : " + start.toString());
            temp = msg.getEnd().getTime() + giorno;
            Date end = new Date();
            end.setTime(temp);
            System.out.println("DATA END : " + end.toString());

            /**
             * PARTE RELATIVA AL MAGAZZINO CENTRALE
             */
            if ((msg.getLocation().get(0)).equals(GlobalVariable.MC)) {

                ArrayList<ReportProductsMC> ordersReports = repoMC.findReportProductsMCByProductAndArriveDateBefore(prod, end);
                switch (msg.getGranularity()) {
                    case GlobalVariable.mensile:
                        calculateWithGranularityMC(GlobalVariable.mensile, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.bimestrale:
                        calculateWithGranularityMC(GlobalVariable.bimestrale, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.trimestrale:
                        calculateWithGranularityMC(GlobalVariable.trimestrale, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.semestrale:
                        calculateWithGranularityMC(GlobalVariable.semestrale, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.annuale:
                        calculateWithGranularityMC(GlobalVariable.annuale, msg, ordersReports, prod);
                        break;
                }
            }
            /**
             * PARTE RELATIVA AI MAGAZZINI LOCALI
             */
            else {
                ArrayList<DailyStoreRoom> ordersReports = repoStoreRoom.findDailyStoreRoomByDateBetweenAndLocation(start,end,msg.getLocation().get(0));
                switch (msg.getGranularity()) {
                    case GlobalVariable.mensile :
                        calculateWithGranularityML(GlobalVariable.mensile, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.bimestrale:
                        calculateWithGranularityML(GlobalVariable.bimestrale, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.trimestrale:
                        calculateWithGranularityML(GlobalVariable.trimestrale, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.semestrale:
                        calculateWithGranularityML(GlobalVariable.semestrale, msg, ordersReports, prod);
                        break;
                    case GlobalVariable.annuale:
                        calculateWithGranularityML(GlobalVariable.annuale, msg, ordersReports, prod);
                        break;
                }
            }
        }
    }

    private void calculateWithGranularityMC(int granularity, MessageIndexCompute msg, ArrayList<ReportProductsMC> ordersReports, Product prod) {
        long giorno = 86400000;
        double quantitaInit = 0;
        double quantitaTotGiorni = 0;
        Date prova = new Date();
        ArrayList<Double> resProd = new ArrayList<>();

        //Calendari istanziati
        GregorianCalendar calStart = new GregorianCalendar();
        calStart.setTime(msg.getStart());
        GregorianCalendar calEnd = new GregorianCalendar();
        calEnd.setTime(msg.getEnd());
        prova.setTime(calStart.getTimeInMillis());

        while (calStart.getTimeInMillis() <= calEnd.getTimeInMillis()) {

            prova.setTime(calStart.getTimeInMillis());

            GregorianCalendar test = new GregorianCalendar();
            test.setTime(calStart.getTime());
            test.add(Calendar.MONTH, granularity);
            long ggDist = (test.getTimeInMillis() - calStart.getTimeInMillis()) / giorno;
            quantitaTotGiorni = 0;

            //Se il mese successivo supera la fine dell'analisi si esce dal ciclo
            if (test.getTimeInMillis() > calEnd.getTimeInMillis()) {
                break;
            }
            for (ReportProductsMC r : ordersReports) {
                quantitaInit = r.getQuantity();
                if (calStart.getTimeInMillis() <= r.getArriveDate().getTime() && r.getArriveDate().getTime() < test.getTimeInMillis()) {
                    for (Order ord : r.getOrders()) {
                        //ELIMINA GLI ORDINI EVASI PRIMA DELLA DATA DI START'
                        if (ord.getOrderDate().getTime() <= calStart.getTimeInMillis()) {
                            //PORTA ALLA QUANTITà INIZIALE UTIE PER LO STUDIO
                            quantitaInit -= ord.getNum_ordinazioni();
                        }
                    }
                    //test.MONTH;
                    quantitaTotGiorni += quantitaInit * ggDist;
                    for (Order ord : r.getOrders()) {
                        //System.out.println("FOR");

                        if (ord.getOrderDate().getTime() >= calStart.getTimeInMillis() && ord.getOrderDate().getTime() < test.getTimeInMillis()) {
                            long x = (ord.getOrderDate().getTime() - calStart.getTimeInMillis());
                            int d = (int) (x / giorno);
                            quantitaTotGiorni = quantitaTotGiorni - ord.getNum_ordinazioni() * (ggDist - d);
                        }
                    }
                }
            }
            calStart.add(Calendar.MONTH, granularity);
            prova.setTime(calStart.getTimeInMillis());
            //DA CAPIRE SE AGGIUNGERE /GRANULARITY
            double giacMedia = (quantitaTotGiorni / ggDist);

            resProd.add(giacMedia);
        }
        resultProduct.add(new Result(resProd));
    }

    private void calculateWithGranularityML(int granularity, MessageIndexCompute msg, ArrayList<DailyStoreRoom> ordersReports, Product prod){

        Date prova = new Date();
        ArrayList<Double> resProd = new ArrayList<>();

        //Calendari istanziati
        GregorianCalendar calStart = new GregorianCalendar();
        calStart.setTime(msg.getStart());
        GregorianCalendar calEnd = new GregorianCalendar();
        calEnd.setTime(msg.getEnd());
        prova.setTime(calStart.getTimeInMillis());

        while (calStart.getTimeInMillis() <= calEnd.getTimeInMillis()) {

            GregorianCalendar test = new GregorianCalendar();
            test.setTime(calStart.getTime());
            test.add(Calendar.MONTH, granularity);

            //Se il mese successivo supera la fine dell'analisi si esce dal ciclo
            if (test.getTimeInMillis() > calEnd.getTimeInMillis()) {
                break;
            }
            int counter=0;
            double stock=0;
            for (DailyStoreRoom r : ordersReports) {
                //quantitaInit = r.getQuantity();
                if (calStart.getTimeInMillis() <= r.getDate().getTime() && r.getDate().getTime() < test.getTimeInMillis()) {
                    for (ItemStore item : r.getItemStores()) {
                        if (item.getProduct().getDescription().equals(prod.getDescription())) {
                            stock+=item.getStock();
                            counter++;
                        }
                    }

                }
            }
            calStart.add(Calendar.MONTH, granularity);
            //PER PRINT
            double giacMedia;
            if(counter==0){
                giacMedia=0;
            }
            else{
                giacMedia = (stock/counter);
            }

            resProd.add(giacMedia);
        }
        resultProduct.add(new Result(resProd));
    }
}
