package com.isssr_backend.variables;

import com.isssr_backend.entity.food.CategoryFood;
import com.isssr_backend.entity.message.MessageIndexCompute;
import com.isssr_backend.entity.product.Category;
import com.isssr_backend.global.GlobalVariable;
import com.isssr_backend.repository.*;
import com.isssr_backend.service.serviceFood.ServiceFoods;
import com.isssr_backend.service.serviceProduct.ServiceProduct;
import com.isssr_backend.variables.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Questa classe astratta specifica il getter del valore e della semantica della variabile stessa
 */
@Component
public abstract class Variable {

    protected String semantic = "missing";

    //Variabile che conterrà il risultato in caso di singolo prodotto
    protected ArrayList<Result> resultProduct = new ArrayList<>();

    //Variabile che conterrà il risultato in caso di categoria
    protected ArrayList<Result> resultCategories = new ArrayList<>();

    protected ArrayList<Result> resultGlobal =  new ArrayList<>();


    @Autowired
    RepositoryDailyStoreRoom repoStoreRoom;

    @Autowired
    RepositoryReportProductsMC repoMC;

    @Autowired
    RepositoryReportProductsMLs repoML;

    @Autowired
    RepositoryDailyReportFoods repoDRF;

    @Autowired
    RepositoryProduct repoPro;

    @Autowired
    RepositoryCategory repoCat;

    @Autowired
    RepositoryCategoryFood repoCategoryFood;

    @Autowired
    ServiceProduct servPr;

    @Autowired
    ServiceFoods serviceFoods;


    @Autowired
    RepositoryDailyStoreRoom repositoryDailyStoreRoom;


    /**
     * Questa funzione prende una lista di valori double e date e li accorpa secondo
     * le seguenti possibili granularità.
     * 1  - mensile
     * 3  - trimestrale
     * 4  - quadrimestrale
     * 6  - semestrale
     * 12 - annuale
     *
     * @param granularity Indice della granularità
     */
    public void applyGranularity(int granularity) {

    }

    /**
     * Questa chiamata impone che ogni classe di tipo Variable ritorni tramite
     * un calcolo specifico per ogni classe un double che rappresenti il valore
     * di tale variabile.
     *
     * @return valore della variabile
     */
    public ArrayList<Result> getValue() {
        return this.resultProduct;
    }


    /**
     * Questa chiamata impone che ogni classe di tipo Variable ritorni tramite
     * un calcolo specifico per ogni classe un double che rappresenti il valore
     * di tale variabile.
     *
     * @return valore della variabile
     */
    public ArrayList<Result> getValueCategory() {
        return this.resultCategories;
    }

    public ArrayList<Result> getValueEconomics() {
        return this.resultCategories;
    }


    /**
     * Questa funzione testa se la semantica di una certa variabile è compatibile con
     * una semantica fornita come parametro.
     *
     * @param sem semantica da testare
     * @return true se la semantica è valida, false altrimenti
     */
    public Boolean testSemantic(String sem) {
        // sempre valida
        if (this.semantic.equals("all")) {
            return true;
        }

        // testa la semantica
        if (sem.toLowerCase().contains(this.semantic.toLowerCase())) {
            return true;
        } else {
            return false;
        }
    }

    //Metodo specifico per e variabili dei Food
    abstract public void computeFood(MessageIndexCompute msg);

    public void computeCategoryFood(MessageIndexCompute msg) {

        System.err.println("MSG:" + msg.getUtente());
        System.err.println("MSG:" + msg.getDescProduct());
        System.err.println("MSG:" + msg.getLocation());


        // lista delli prodotti presenti nella categoria attuale
        ArrayList<String> foodsInCategory = new ArrayList<>();

        // per ogni descrittore del messaggio
        for (int i = 0; i < msg.getDescProduct().size(); i++) {

            // reset del contenuto delle liste sui prodotti
            resultProduct.clear();
            foodsInCategory.clear();

            //System.out.println("desc : " + msg.getDescProduct().get(i));
            // carica la categoria dal DB
            CategoryFood category = repoCategoryFood.findCategoryFoodByNameEqualsAndLocationAndUtente(msg.getDescProduct().get(i)
                    , msg.getLocation().get(0), msg.getUtente());
            //System.out.println("SONO NULL");
            if (category == null) {
                System.out.println("SONO NULL");
                //categoria standard
                category = repoCategoryFood.findCategoryFoodByNameEqualsAndLocationAndUtente(msg.getDescProduct().get(i),
                        msg.getLocation().get(0), GlobalVariable.standard);
            }
            //System.out.println("size : " + category.getFood().size());


            // poniamo in lista tutti i prodotti della specifica categoria
            for (int j = 0; j < category.getFood().size(); j++) {
                //System.out.println(category.getFood().get(j).getNome());
                foodsInCategory.add(category.getFood().get(j).getNome());
            }

            // salva la lista di prodotti così generata nel messaggio
            MessageIndexCompute clone = msg.clone();
            clone.setDescProduct(foodsInCategory);

            // passa il messaggio alla variabile perchè si popoli
            computeFood(clone);

            // store all products in the i-th list of the category
            aggregateProducts();
        }
    }

    /// chiamata ad indice economico
    abstract public void computeEconomic(MessageIndexCompute msg);

    // metodo specifico di ogni variabile per il calcolo del valore assunto
    abstract public void computeProduct(MessageIndexCompute msg);

    // metodo per l'aggregamento dei prodotti di una categoria in un solo oggetto
    public void computeCategoryProduct(MessageIndexCompute msg) {

        // lista delli prodotti presenti nella categoria attuale
        ArrayList<String> productsInCategory = new ArrayList<>();

        //System.err.println("MSG:"+msg.getUtente());
        //System.err.println("MSG:"+msg.getDescProduct());
        //System.err.println("MSG:"+msg.getLocation());

        // per ogni descrittore del messaggio
        for (int i = 0; i < msg.getDescProduct().size(); i++) {

            // reset del contenuto delle liste sui prodotti
            resultProduct.clear();
            productsInCategory.clear();

            // carica una categoria dal DB alla volta in base a i
            Category category = repoCat.findCategoryByName(msg.getDescProduct().get(i));

            // Inserimento in lista di tutte le descrizioni dei prodotti della categoria
            for (int j = 0; j < category.getProducts().size(); j++) {
                //System.out.println(category.getProducts().get(j).getDescription());
                productsInCategory.add(category.getProducts().get(j).getDescription());
            }
            // salva la lista di prodotti così generata nel messaggio
            MessageIndexCompute clone = msg.clone();
            clone.setDescProduct(productsInCategory);

            // passa il messaggio alla variabile perchè si popoli
            System.err.println("Quante volte computo la variabile: " + i);
            computeProduct(clone);

            // store all products in the i-th list of the category
            aggregateProducts();
        }
    }

    // funzione atomica per aggregare prodotti in una categoria
    protected void aggregateProducts() {

        // lista di dati
        ArrayList<Double> res = new ArrayList<>();

        // contatore
        double aggregatorCounter = 0;

        // System.out.println("size resulet product :" + resultProduct.size() + " " + resultProduct.get(0).getVar().size() );
        // Seleziona un result alla volta dall'arrayList di result
        Result temp = resultProduct.get(0);
        if (resultProduct.size() > 1) {
            for (int j = 0; j < temp.getVar().size(); j++) {

                aggregatorCounter = 0;
                for (int i = 0; i < resultProduct.size(); i++) {
                    aggregatorCounter += resultProduct.get(i).getVar().get(j);
                }
                res.add(aggregatorCounter);
            }
        } else {
            //Da fidarsi della clone
            res = (ArrayList<Double>) resultProduct.get(0).getVar().clone();
            System.err.println("Size RES " + res.size());
        }

        Result r = new Result();
        r.setVar(res);

        //System.out.println("size res: " + res.size());
        // store the obtained data
        //System.out.println("size categorie result : " +         resultCategories.size());
        resultCategories.add(r);
        System.err.println("SIZE CAtegerories: " + resultCategories.size());
        //System.out.println("size categorie result : " +         resultCategories.size());
    }

    // funzione atomica per aggregare prodotti in una categoria
    protected ArrayList<Result> aggregateProducts2(ArrayList<Result> input) {

        // output
        ArrayList<Result> output = new ArrayList<>();

        // lista di dati
        ArrayList<Double> res = new ArrayList<>();

        // contatore
        double aggregatorCounter = 0;

        // System.out.println("size resulet product :" + resultProduct.size() + " " + resultProduct.get(0).getVar().size() );
        // Seleziona un result alla volta dall'arrayList di result
        Result temp = input.get(0);

        if (input.size() > 1) {
            for (int j = 0; j < temp.getVar().size(); j++) {

                aggregatorCounter = 0;
                for (int i = 0; i < input.size(); i++) {
                    aggregatorCounter += input.get(i).getVar().get(j);
                }
                res.add(aggregatorCounter);
            }
        } else {
            //Da fidarsi della clone
            res = (ArrayList<Double>) resultProduct.get(0).getVar().clone();
            System.err.println("Size RES " + res.size());
        }

        Result r = new Result();
        r.setVar(res);

        //System.out.println("size res: " + res.size());
        // store the obtained data
        //System.out.println("size categorie result : " +         resultCategories.size());
        output.add(r);
        System.err.println("SIZE CAtegerories: " + output.size());
        //System.out.println("size categorie result : " +         resultCategories.size());

        return output;
    }


}
