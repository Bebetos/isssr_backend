package com.isssr_backend.global;

/**
 * Created by andre on 02/07/2017.
 */
public class GlobalVariable {

    //Blocco variabili di granularità
    static final public int mensile = 1;
    static final public int bimestrale = 2;
    static final public int trimestrale = 3;
    static final public int semestrale = 6;
    static final public int annuale = 12;
    static final public String standard = "STD";
    static final public String date = "date";
    static final public int dayInSec = 86400;
    //Variable batch MC
    static final public String id = "id";
    static final public String deliveryDate = "delDate";
    static final public String expiredDate = "expDate";
    static final public String product = "product";
    static final public String index = "index";
    static final public String name = "name";
    static final public String description = "description";
    static final public String brand = "brand";
    static final public String category = "category";
    static final public String remaining = "remaining";
    static final public String quantity = "quantity";
    static final public String commission = "commission";
    static final public String source = "source";
    static final public String price = "price";
    static final public String outBatches = "outBatches";
    static final public String destination = "destination";
    //Categoria Standard
    static final public String stdCategory = "Categoria standard";
    static final public String topManager = "Top Manager";
    static final public String headQuarter = "HeadQuarter";
    static final public String MC = "Magazzino Centrale";

    //Ruoli
    static final public String ManagerLocale="Manager Locale";
    static final public String ManagerCentrale="Manager Centrale";
    static final public String TopManager="Top Manager";
    static final public String RomaCentro="Roma Centro";

}
