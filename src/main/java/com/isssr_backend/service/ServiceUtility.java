package com.isssr_backend.service;

import com.isssr_backend.entity.MyDate;
import com.isssr_backend.entity.PointOfSale;
import com.isssr_backend.entity.food.*;
import com.isssr_backend.entity.product.*;
import com.isssr_backend.global.GlobalVariable;
import com.isssr_backend.repository.*;
import com.isssr_backend.rest.DownloadData;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * Created by andre on 15/06/2017.
 */
@Service
public class ServiceUtility {


    @Autowired
    RepositoryCategory mRepoCat;

    @Autowired
    RepositoryProduct mRepoProd;

    @Autowired
    RepositoryReportProductsMLs mRepo_Mls;

    @Autowired
    RepositoryReportProductsMC mRepo_MC;

    @Autowired
    RepositoryPointOfSale mRepo_POS;

    @Autowired
    RepositoryDailyStoreRoom mRepoDailyStoreRoom;

    @Autowired
    RepositoryDailyReportFoods mReportDailyFood;

    @Autowired
    RepositoryFood mRepositoryFood;

    @Autowired
    RepositoryCategoryFood mRepoCategoryFood;

    @Autowired
    RepositoryCommission mRepoCommission;

    @Autowired
    RepositoryBatch mRepoBatch;

    @Autowired
    RepositoryBatchRelation mRepoBatchRel;

    Timer timer;

    /**
     * Funzione usata esclusivamente per il TESTING, che mappa randomicamente prodotti in categorie
     *
     * @return
     */
    public String updateCategorie() {

        List<Category> mCatList = mRepoCat.findAll();
        List<Product> mProdList = mRepoProd.findAll();
        Random rand = new Random();

        for (int i = 0; i < mProdList.size(); i++) {
            int numberCat = rand.nextInt(10) + 2;
            Product mProd = mProdList.get(i);
            for (int j = 0; j < numberCat; j++) {
                int indexCat = rand.nextInt(mCatList.size());
                Category mCat = mCatList.get(indexCat);
                mCat.addProduct(mProd);
                mRepoCat.save(mCat);
            }
        }
        return "Prodotti aggiunti alle rispettive categorie";
    }


    /**
     * Function che riceve tutti i batchRelation e li inserisce nel nostro DB parsando la JSON string per popolare le nostre classi
     * ReportProductsMC, ReportProductsMLs con i relativi oggetti.
     *
     * @return String Success (Per ora)
     * @throws IOException
     */
    public String updateBatchREL() throws IOException {

        DownloadData x = new DownloadData();

        String json = x.dataBatchRelCentrale();

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        ReportProductsMC reportMc = new ReportProductsMC();
        String dailyStoreRoomDone = "";

        for (int i = 0; i < jsonArray.length(); i++) {

            try {
                JSONObject batchPadre = jsonArray.getJSONObject(i).getJSONObject("batch");
                //System.out.println("Oggetto parsato:" + batchPadre);
                //Setting ID del reportMc come ID del Batch
                reportMc.setId(batchPadre.getString(GlobalVariable.id));

                //Blocco che parsa una stringa in una data
                long arriveDate = (long) batchPadre.get(GlobalVariable.deliveryDate);
                Date dt = new Date(arriveDate);
                dt.setHours(0);
                dt.setMinutes(0);
                dt.setSeconds(0);
                SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                //System.out.println(sfd.format(dt));
                reportMc.setArriveDate(dt);
                //System.out.println("Data di arrivo: "+reportMc.getArriveDate());

                //Blocco data scadenza
                long expDate = (long) batchPadre.get(GlobalVariable.expiredDate);
                dt = new Date(expDate);
                dt.setHours(0);
                dt.setMinutes(0);
                dt.setSeconds(0);
                reportMc.setExpiredDate(dt);
                //System.out.println("Data di scadenza: "+reportMc.getExpiredDate());

                //Parsing del product nel padre
                JSONObject product = batchPadre.getJSONObject(GlobalVariable.product);
                Product prod = new Product();
                int index = product.getInt(GlobalVariable.index);
                String name = product.getString(GlobalVariable.name);
                String description = product.getString(GlobalVariable.description);
                String brand = product.getString(GlobalVariable.brand);
                String id = product.getString(GlobalVariable.id);
                prod.setName(name);
                prod.setDescription(description);
                prod.setBrand(brand);
                prod.setIndex(index);
                prod.setId(id);

                //BLOCCO Parsing delle categorie
                ArrayList<String> categorie = new ArrayList<>();
                JSONArray categories = product.getJSONArray(GlobalVariable.category);
                for (int k = 0; k < categories.length(); k++) {
                    String cat1 = categories.getString(k);
                    categorie.add(cat1);
                    Category category;
                    //System.out.println("CAtegorie"+cat1);
                    category = mRepoCat.findCategoryByName(cat1);
                    //codice che gestisce la creazione di una nuova categoria associando il prodotto ad essa
                    if (category == null) {
                        //System.out.println("CATEGORIA STANDARD");
                        category = new Category();
                        category.setName(cat1);
                        category.setDescription(GlobalVariable.stdCategory);
                        category.setUtente(GlobalVariable.standard);
                        ArrayList<Product> p = new ArrayList<>();
                        p.add(prod);
                        Set<Product> productDistict = new HashSet<>();
                        productDistict.addAll(p);
                        p.clear();
                        p.addAll(productDistict);
                        category.setProducts(p);
                        mRepoCat.save(category);
                        //Codice che gestisce l'esistenza di una categoria e l'aggiunta del prodotto alla categoria gia eistente
                    } else {
                        //System.out.println("CATEGORIA GIA ESISTENTE");

                        ArrayList<Product> p;
                        p = category.getProducts();
                        p.add(prod);

                        Set<Product> productDistict = new HashSet<>();
                        productDistict.addAll(p);
                        p.clear();
                        p.addAll(productDistict);
                        category.setProducts(p);
                        mRepoCat.save(category);
                    }
                }
                prod.setCategory(categorie);
                reportMc.setProduct(prod);

                //Parsing del campo stock
                int stock = batchPadre.getInt(GlobalVariable.remaining);
                reportMc.setStock(stock);

                //Parsing del campo quantity
                int quantity = batchPadre.getInt(GlobalVariable.quantity);
                reportMc.setQuantity(quantity);

                JSONObject commission = batchPadre.getJSONObject(GlobalVariable.commission);
                //Blocco che parsa una stringa in una data
                long orderDate = (long) commission.get(GlobalVariable.date);
                dt = new Date(orderDate);
                dt.setHours(0);
                dt.setMinutes(0);
                dt.setSeconds(0);
                sfd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                //System.out.println(sfd.format(dt));
                reportMc.setOrderDate(dt);
                //System.out.println("Data dell'ordine: "+reportMc.getOrderDate());
                //Setting supplier
                reportMc.setSupplier(commission.getString(GlobalVariable.source));
                //Setting prezzo di ingresso al magazzino centrale
                reportMc.setCosto(batchPadre.getDouble(GlobalVariable.price));
                //System.out.println("Costo del batch: "+ reportMc.getCosto());


                //Parsing Degli out Batches
                JSONArray outBatches = jsonArray.getJSONObject(i).getJSONArray(GlobalVariable.outBatches);
                //System.out.println("OUTBATCHES: "+outBatches);

                ArrayList<Order> orders = new ArrayList<>();
                for (int j = 0; j < outBatches.length(); j++) {
                    Order order = new Order();
                    ReportProductsMLs prodMLs = new ReportProductsMLs();
                    JSONObject outBatch = outBatches.getJSONObject(j);
                    //System.out.println("Costo del batch: "+ outBatch);

                    //Data di consegna dell'ordine
                    long arrivedDate = (long) batchPadre.get(GlobalVariable.deliveryDate);
                    dt = new Date(arrivedDate);
                    dt.setHours(0);
                    dt.setMinutes(0);
                    dt.setSeconds(0);
                    //System.out.println(sfd.format(dt));
                    order.setOrderDate(dt);
                    //Setting nel prodotto magazzino locale della data
                    prodMLs.setArriveDate(dt);
                    //Setting della data di scadenza
                    prodMLs.setExpiredDate(reportMc.getExpiredDate());
                    //Setting della quantità
                    order.setNum_ordinazioni(outBatch.getInt(GlobalVariable.quantity));
                    prodMLs.setQuantity(outBatch.getInt(GlobalVariable.quantity));
                    //Setting della quantità rimanente
                    prodMLs.setStock(outBatch.getInt(GlobalVariable.remaining));
                    // Setting del prezzo di vendita
                    prodMLs.setCosto(outBatch.getDouble(GlobalVariable.price));
                    order.setSellPrice(outBatch.getDouble(GlobalVariable.price));
                    // Setting dell'ID del prodotto ML con l'ID dell'outBatche
                    prodMLs.setId(outBatch.getString(GlobalVariable.id));
                    //Entriamo nel campo commission
                    JSONObject cOUT = outBatch.getJSONObject(GlobalVariable.commission);
                    //setting id order
                    order.setId(cOUT.getString(GlobalVariable.id));
                    //SEtting del campo destination
                    prodMLs.setPosition(cOUT.getString(GlobalVariable.destination));
                    order.setDestination(cOUT.getString(GlobalVariable.destination));
                    //Setting del campo product nel ML
                    prodMLs.setProduct(reportMc.getProduct());

                    orders.add(order);
                    //Salvataggio oggetto ReportMLs
                   // System.out.println("ProdMLS:" + prodMLs.getQuantity() + prodMLs.getArriveDate() + prodMLs.getStock());
                    mRepo_Mls.save(prodMLs);

                }
                reportMc.setOrders(orders);
                //Salvataggio oggetto ReportMC
                mRepo_MC.save(reportMc);
                //Salvataggio o update di un prodotto ogni volta che lo si trova nel db del magazzino centrale
                mRepoProd.save(prod);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        dailyStoreRoomDone = wrappedStoreRoom();

        // OPERAZIONI CON L'ARRAYLIST DI BATCH RESTITUITO
        return "Update REPORT MC E ML completed" + dailyStoreRoomDone;
    }

    /**
     * Funzione che crea una vista giornaliera del magazzino locale in modo da poter calcolare variabili per il food allo stato
     * attuale dei dati fornitici dall'integrazione. Ci aiuta a tenere la tracciabilità di quello che è presente nel magazzino nel tempo
     *
     * @return
     */
    private String wrappedStoreRoom() {
        ArrayList<PointOfSale> position = mRepo_POS.findAll();

        GregorianCalendar gc = new GregorianCalendar();
        gc.set(Calendar.MINUTE, 0);
        gc.set(Calendar.HOUR_OF_DAY, 0);
        gc.set(Calendar.SECOND, 0);
        gc.set(Calendar.MILLISECOND, 0);

        for (int i = 0; i < position.size(); i++) {
            ArrayList<ItemStore> itemStores = new ArrayList<>();
            ArrayList<ReportProductsMLs> reportProductsMLs = mRepo_Mls.findReportProductsMLsByPosition(position.get(i).getName());


            while (reportProductsMLs.size() != 0) {
                ItemStore itemStore = new ItemStore();
                //itemStore.setProduct(reportProductsMLs.get(0).getProduct());

                int TotalStock = 0;
                double avgCosto = 0.0;

                int n = 0;
                double d = 0;
                int k = 0;
                ReportProductsMLs tmp = reportProductsMLs.get(k);
                //System.out.println("temp prima: "+ tmp.getProduct().getDescription());
                while (k < reportProductsMLs.size()) {
                    //System.out.println("k : " + k);
                    // System.out.println("NEL WHILE"+ tmp.getProduct().getDescription()+"=== "+reportProductsMLs.get(k).getProduct().getDescription());
                    if (tmp.getProduct().getDescription().equals(reportProductsMLs.get(k).getProduct().getDescription())&&reportProductsMLs.get(k).getExpiredDate().getTime()<gc.getTimeInMillis()) {
                        System.err.println("ENTRO NELL'IF");
                        TotalStock = TotalStock + reportProductsMLs.get(k).getStock();
                        n++;
                        d = (reportProductsMLs.get(k).getCosto() - avgCosto);
                        avgCosto = avgCosto + d / n;

                        reportProductsMLs.remove(k);
                        k = 0;
                        //System.out.println("NELL'IF: "+avgCosto);
                        //System.out.println( "size" + reportProductsMLs.size());

                    }else{
                        if (reportProductsMLs.get(k).getExpiredDate().getTime() >= gc.getTimeInMillis()) {
                            reportProductsMLs.remove(k);
                            k=0;
                        }
                        k++;
                    }
                }
                //System.out.println("temp dopo: "+ tmp.getProduct().getDescription());
                //System.err.println("SET ITEM");
                itemStore.setProduct(tmp.getProduct());
                itemStore.setAvgCosto(avgCosto);
                itemStore.setStock(TotalStock);
                itemStores.add(itemStore);

            }
            System.err.println("SET DAILY");
            DailyStoreRoom daily = new DailyStoreRoom();
            daily.setLocation(position.get(i).getName());
            daily.setDate(gc.getTime());
            daily.setItemStores(itemStores);


            DailyStoreRoom tmp = mRepoDailyStoreRoom.findDailyStoreRoomByDateIsAndLocation(daily.getDate(), daily.getLocation());
            if (tmp != null) {
                System.err.println("Save1");
                daily.setId(tmp.getId());
                mRepoDailyStoreRoom.save(daily);
            } else {
                System.err.println("Save2");
                mRepoDailyStoreRoom.save(daily);
            }
        }

        return "UpdateCompleted";

    }

    /**
     * Function che riceve tutti i point of sale e aggiorna il DB
     *
     * @return String Success (Per ora)
     * @throws IOException
     */
    public String updatePos() throws IOException {

        DownloadData x = new DownloadData();

        String json = x.getAllPos();

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                PointOfSale sale = new PointOfSale();
                JSONObject pos = jsonArray.getJSONObject(i);
                sale.setName(pos.getString("name"));
                sale.setDistance(pos.getDouble("distance"));
                sale.setId(pos.getString("id"));
                sale.setIva(pos.getString("iva"));
                //System.out.println("POS: " + sale.getIva() + " " + sale.getName());
                mRepo_POS.save(sale);


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        return "Update POS completed";
    }

    /**
     * Funzione che effettua una chiamata GET per ricevere i dailyReportFoods e li parsa in Json nelle nostre classi Bucket e DailyReportFood
     *
     * @return
     */
    public String updateDailyReportFoods() {

        DownloadData x = new DownloadData();
        GregorianCalendar gc = new GregorianCalendar();
        gc.set(Calendar.MINUTE, 0);
        gc.set(Calendar.HOUR_OF_DAY, 0);
        gc.set(Calendar.SECOND, 0);
        //System.out.println(gc.getTime().toString());

        String json = x.dataDailyReport(gc);
        //System.out.println("JSON: " + json);

        // MANCA IL PARSER VERO E PROPRIO DEI PRODOTTI

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        ReportProductsMC reportMc = new ReportProductsMC();
        String dailyStoreRoomDone = "";

        Date dt;

        //istanzio output
        Foods foods = new Foods();
        ItemDailyReport itemDailyReport = new ItemDailyReport();
        ReportDailyFoods reportDailyFoods = new ReportDailyFoods();
        ArrayList<ItemDailyReport> itemDailyReportArrayList = new ArrayList<>();
        CategoryFood categoryFood = new CategoryFood();


        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject dailyReport = jsonArray.getJSONObject(i);
                long date = (long) dailyReport.get("data");
                dt = new Date(date);
                dt.setHours(0);
                dt.setMinutes(0);
                dt.setSeconds(0);
                SimpleDateFormat sfd = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                String location = dailyReport.getString("location");

                reportDailyFoods.setDate(dt);
                reportDailyFoods.setLocation(location);

                //settiamo le variabili dentro l'arrayList ItemDailyReport
                JSONArray pietanzamenu = dailyReport.getJSONArray("pietanzaAnalyticsDtos");
                for (int j = 0; j < pietanzamenu.length(); j++) {
                    JSONObject pietanza = pietanzamenu.getJSONObject(j);
                    int produced = pietanza.getInt("preparato");
                    int sale = pietanza.getInt("venduto");
                    String mainCategory = pietanza.getString("categoria");
                    categoryFood.setName(mainCategory);
                    categoryFood.setLocation(location);
                    categoryFood.setUtente(GlobalVariable.standard);
                    itemDailyReport.setQuantitaConsumata(sale);
                    itemDailyReport.setQuantitaProdotta(produced);

                    JSONObject pietanzaMenu = pietanza.getJSONObject("pietanzaMenuDto");
                    JSONObject pietanzaFood = pietanzaMenu.getJSONObject("pietanzaDto");

                    String nome = pietanzaFood.getString("nome");
                    double price = pietanzaFood.getDouble("prezzo");
                    ArrayList<String> etichette = new ArrayList<>();
                    JSONArray tags = pietanzaFood.getJSONArray("etichette");
                    for (int k = 0; k < tags.length(); k++) {
                        etichette.add(tags.getString(k));
                    }
                    JSONArray ingredienti = pietanzaFood.getJSONArray("ingredienti");
                    ArrayList<Ingrediente> ingr = new ArrayList<>();
                    for (int k = 0; k < ingredienti.length(); k++) {
                        Ingrediente tmp = new Ingrediente();
                        tmp.setNomeProdotto(ingredienti.getJSONObject(k).getString("nomeProdotto"));
                        tmp.setQuantita(ingredienti.getJSONObject(k).getInt("quantita"));
                        ingr.add(tmp);
                    }

                    foods.setEtichette(etichette);
                    foods.setIngredienti(ingr);
                    foods.setNome(nome);
                    foods.setMainCategory(mainCategory);
                    foods.setLocation(location);

                    Foods f = mRepositoryFood.findFoodsByNomeAndLocation(nome, location);
                    if (f != null) {
                        foods.setId(f.getId());
                        mRepositoryFood.save(foods);
                    } else mRepositoryFood.save(foods);

                    ArrayList<Foods> tmp = new ArrayList<>();
                    tmp.add(foods);

                    categoryFood.setDescription(mainCategory);
                    categoryFood.setFood(new ArrayList<Foods>());
                    CategoryFood c = mRepoCategoryFood.findCategoryFoodByNameEqualsAndLocation(mainCategory, location);

                    if (c != null) {
                        int s = 0;
                        boolean find = false;
                        while (s < c.getFood().size()) {
                            if (c.getFood().get(s).getNome().equals(foods.getNome())) {
                                find = true;
                                break;
                            }
                            s++;
                        }
                        if (find == false) {
                            c.getFood().add(foods);
                            mRepoCategoryFood.save(c);
                        }
                    } else {
                        categoryFood.setFood(tmp);
                        mRepoCategoryFood.save(categoryFood);
                    }

                    itemDailyReport.setFoods(foods);
                    itemDailyReport.setPrice(price);
                    itemDailyReport.setQuantitaConsumata(sale);
                    itemDailyReport.setQuantitaProdotta(produced);

                    itemDailyReportArrayList.add(itemDailyReport);

                }

                reportDailyFoods.setFoodSale(itemDailyReportArrayList);
                ReportDailyFoods tmpDf = mReportDailyFood.findReportDailyFoodsByDateAndLocation(dt, location);
                if (tmpDf != null) {
                    reportDailyFoods.setId(tmpDf.getId());
                    mReportDailyFood.save(reportDailyFoods);
                }
                mReportDailyFood.save(reportDailyFoods);


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        return "UPDATE DAILY REPORT FOOD COMPLETED";

    }

    /**
     * This function is used for filling the database with some helpfull data.
     *
     * @param mDate a MyDate with date information.
     * @param prodIndex the ArrayList index for the product (WARNING! no control on it).
     * @return the BatchRelation on successfull save.
     */
    public BatchesRelation createRandomBatchRelation(MyDate mDate, int prodIndex) {
        Commission fCommission = new Commission();
        Commission sCommission = new Commission();

        int afterDel = 3;
        int monthExp = 1;

        fCommission.setDate(new Date(mDate.getYear(), mDate.getMonth(), mDate.getDay(), mDate.getHour(), mDate.getMinute(), mDate.getSeconds()));
        fCommission.setDeliveryTime(Integer.toString(afterDel));
        fCommission.setSource("EXT");
        fCommission.setDestination("MC");
        fCommission.setCompleted("y");

        sCommission.setDate(new Date(mDate.getYear(), mDate.getMonth(), mDate.getDay()+1, mDate.getHour(), mDate.getMinute(), mDate.getSeconds()));
        sCommission.setDeliveryTime(Integer.toString(afterDel));
        sCommission.setSource("MC");
        sCommission.setDestination("Roma Centro");
        sCommission.setCompleted("y");

        fCommission = mRepoCommission.save(fCommission);
        sCommission = mRepoCommission.save(sCommission);

        Product mProd = mRepoProd.findAll().get(prodIndex);

        Batch fBatch = new Batch();
        Batch sBatch = new Batch();

        double price = new Random().nextDouble();
        price = price * 100.0;
        int remain = new Random().nextInt(100);

        fBatch.setCommission(fCommission);
        fBatch.setDelDate(new Date(mDate.getYear(), mDate.getMonth(), mDate.getDay()+afterDel, mDate.getHour(), mDate.getMinute(), mDate.getSeconds()));
        fBatch.setExpDate(new Date(mDate.getYear(), mDate.getMonth() + monthExp, mDate.getDay()+1, mDate.getHour(), mDate.getMinute(), mDate.getSeconds()));
        fBatch.setProduct(mProd);
        fBatch.setPrice(Math.round(price * 100.0) / 100.0);
        fBatch.setQuantity(100);
        fBatch.setRemaining(remain);
        fBatch.setDelivered("yes");

        fBatch = mRepoBatch.save(fBatch);

        int diff = new Random().nextInt(remain);
        price += (new Random().nextDouble() * 50);

        sBatch.setCommission(sCommission);
        sBatch.setDelDate(new Date(mDate.getYear(), mDate.getMonth(), mDate.getDay()+afterDel, mDate.getHour(), mDate.getMinute(), mDate.getSeconds()));
        sBatch.setExpDate(new Date(mDate.getYear(), mDate.getMonth() + monthExp, mDate.getDay()+1, mDate.getHour(), mDate.getMinute(), mDate.getSeconds()));
        sBatch.setProduct(mProd);
        sBatch.setPrice(Math.round(price * 100.0) / 100.0);
        sBatch.setQuantity(remain);
        sBatch.setRemaining(diff);
        sBatch.setDelivered("yes");

        sBatch = mRepoBatch.save(sBatch);

        ArrayList<Batch> mOutBatch = new ArrayList();
        mOutBatch.add(sBatch);

        BatchesRelation mBatchRel = new BatchesRelation();

        mBatchRel.setBatch(fBatch);
        mBatchRel.setOutBatches(mOutBatch);

        return mRepoBatchRel.save(mBatchRel);
    }

}
