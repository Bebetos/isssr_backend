package com.isssr_backend.service.serviceFood;

import com.isssr_backend.entity.food.CategoryFood;
import com.isssr_backend.entity.food.Foods;
import com.isssr_backend.entity.message.MessageNewCategoryFood;
import com.isssr_backend.global.GlobalVariable;
import com.isssr_backend.repository.RepositoryCategoryFood;
import com.isssr_backend.repository.RepositoryFood;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by GM on 04/07/2017.
 */
@Service
public class ServiceCategoryFood {

    @Autowired
    private RepositoryCategoryFood mRepo_categoryFood;

    @Autowired
    private RepositoryFood mRepo_Food;

    public ArrayList<CategoryFood> getAll() {
        return mRepo_categoryFood.findAll();
    }


    /**
     * Utilizzata per riempire la select dei food, richiamata dal ControllerCategoryFood
     *
     * @param nameCategory
     * @return ArrayList<Foods>
     */
    public ArrayList<Foods> riempiSelectFood(String nameCategory) {

        CategoryFood c = mRepo_categoryFood.findCategoryFoodByNameEquals(nameCategory);

        ArrayList<Foods> allFood = c.getFood();
        return allFood;
    }


    /**
     * Funzione che restituisce un'ArrayList di categorie relative alle pietanze in base ai parametri sotto elencati provenienti dal front-end
     *
     * @param str      (Possibilità di essere ""
     * @param utente
     * @param location
     * @return ArrayList<CategoryFood>
     */
    public ArrayList<CategoryFood> riempiSelectIndexCategory(String str, String utente, String location) {

        return mRepo_categoryFood.findCategoryByNameContainingAndUtenteAndLocationOrUtenteAndLocationAndNameContaining(str, utente, location, GlobalVariable.standard, location, str);
    }


    /**
     * Aggiunge una nuova cateogria personalizzata visibile solo all'utente che l'ha generata
     *
     * @param x
     * @return CategoryFood appena creata
     */
    public CategoryFood addCustomCategory(MessageNewCategoryFood x) {

        CategoryFood retFood = new CategoryFood();
        retFood.setName(x.getName());
        retFood.setDescription(x.getDescription());
        retFood.setUtente(x.getUtente());
        retFood.setLocation(x.getLocation());

        ArrayList<String> food = new ArrayList<>();
        if (x.getFood() != null) {
            food = x.getFood();
        }
        ArrayList<String> catFood = new ArrayList<>();
        if (x.getCategorieFood() != null) {
            catFood = x.getCategorieFood();
        }
        ArrayList<Foods> foods = new ArrayList<>();
        if (food.size() != 0) {
            for (int k = 0; k < food.size(); k++) {
                Foods f = mRepo_Food.findFoodsByNomeAndLocation(food.get(k), x.getLocation());
                if (f != null) {
                    foods.add(f);
                }
            }
        }
        if (catFood.size() != 0) {
            for (int z = 0; z < catFood.size(); z++) {
                ArrayList<CategoryFood> c = mRepo_categoryFood.findCategoryFoodByNameAndUtenteOrUtenteAndName(catFood.get(z),
                        x.getUtente(), GlobalVariable.standard, catFood.get(z));
                if (!(x.getUtente()).equals(GlobalVariable.topManager)) {
                    for (CategoryFood cf : c) {
                        if (cf.getLocation().equals(x.getLocation())) {
                            foods.addAll(cf.getFood());
                        }
                    }
                } else {
                    for (CategoryFood cf : c) {
                        foods.addAll(cf.getFood());
                    }
                }
            }
        }
        Set<Foods> FoodsDistict = new HashSet<>();
        FoodsDistict.addAll(foods);
        foods.clear();
        foods.addAll(FoodsDistict);
        retFood.setFood(foods);

        CategoryFood cf = mRepo_categoryFood.findCategoryFoodByNameEqualsAndLocationAndUtente(x.getName(), x.getLocation(), x.getUtente());
        if (cf != null) {
            cf.setFood(foods);
            mRepo_categoryFood.save(cf);
        } else {
            mRepo_categoryFood.save(retFood);
        }
        return retFood;
    }

    public ArrayList<CategoryFood> findAll() {
        return mRepo_categoryFood.findAll();
    }

    public void deleteCategoryFood(String name,String utente,String location){

    mRepo_categoryFood.removeCategoryByNameAndUtenteAndLocationOrUtenteAndLocationAndName(name, utente, location, GlobalVariable.standard, location, name);
    }

    public ArrayList<Foods> getFoodByCatLoc(String category,String location){

        CategoryFood cat= mRepo_categoryFood.findCategoryFoodByNameAndLocation(category,location);

        if(cat!=null){
         return cat.getFood();
        }

        return null;
    }

}
