package com.isssr_backend.service;

import com.isssr_backend.entity.TopThree;
import com.isssr_backend.entity.food.Foods;
import com.isssr_backend.entity.message.MessageIndexCompute;
import com.isssr_backend.entity.product.Product;
import com.isssr_backend.global.GlobalVariable;
import com.isssr_backend.repository.RepositoryFood;
import com.isssr_backend.repository.RepositoryProduct;
import com.isssr_backend.repository.RepositoryTopThree;
import com.isssr_backend.variables.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by andre on 09/07/2017.
 */
@Service
public class ServiceMainProduct {

    @Autowired
    private RepositoryProduct repositoryProduct;

    @Autowired
    private ServiceIndex serviceIndex;

    @Autowired
    private RepositoryTopThree topThree;

    @Autowired
    private RepositoryFood repositoryFood;


    /**
     * In futuro è possibile scegliere l'index da calcolare. Per ora è impostato quello di default Costo.
     *
     * @return
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    public TopThree topThreeIndexProduct(String index) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {


        //DATA ODIERNA
        GregorianCalendar today = new GregorianCalendar();
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        today.add(Calendar.DAY_OF_MONTH, -1);

        // Cached on database
        TopThree ret = topThree.findTopThreeByDateAndIndex(today.getTime(), index);
        if (ret != null) {
            return ret;
        }

        MessageIndexCompute msg = new MessageIndexCompute();

        //Setting Stringhe descrittori di tutti i prodotti
        ArrayList<String> productDesc = new ArrayList<>();
        ArrayList<Product> allProds = repositoryProduct.findAll();
        for (int i = 0; i < allProds.size(); i++) {
            productDesc.add(allProds.get(i).getDescription());
        }
        msg.setDescProduct(productDesc);

        //Set time start
        GregorianCalendar start = new GregorianCalendar();
        GregorianCalendar end = new GregorianCalendar();
        start.set(Calendar.HOUR, 0);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);
        start.set(Calendar.MILLISECOND, 0);
        start.add(Calendar.MONTH, -1);
        start.add(Calendar.DAY_OF_MONTH, -2);
        msg.setStart(start.getTime());

        //Set time end
        end.set(Calendar.HOUR, 0);
        end.set(Calendar.MINUTE, 0);
        end.set(Calendar.SECOND, 0);
        end.set(Calendar.MILLISECOND, 0);
        msg.setEnd(end.getTime());

        //Set location
        ArrayList<String> location = new ArrayList<>();
        location.add(GlobalVariable.RomaCentro);
        msg.setLocation(location);


        msg.setGranularity(1);
        msg.setFlag("");

        //Set Index
        msg.setIndex(index);

        ArrayList<Result> result = serviceIndex.computeIndex(msg);
        ArrayList<Double> bests = new ArrayList<>();
        ArrayList<String> bestStr = new ArrayList<>();

        int j = 0;
        while (j < 3) {
            double best = 0;
            String name = "";
            int indexMax = 0;
            for (int i = 0; i < result.size(); i++) {
                double var = result.get(i).getVar().get(0);
                System.err.println("result name:" + msg.getDescProduct().get(i));
                System.err.println("result value:" + var);
                if (var >= best) {
                    best = var;
                    name = msg.getDescProduct().get(i);
                    indexMax = i;
                }
            }
            j++;
            System.err.println("Nome:" + name + " - Valore: " + best);
            bestStr.add(name);
            bests.add(best);
            msg.getDescProduct().remove(indexMax);
            result.remove(indexMax);
        }

        TopThree top = new TopThree();
        if (bests.size() == 3 && bestStr.size() == 3) {


            top.setFirst(bestStr.get(0).concat(" " + bests.get(0)));
            top.setSecond(bestStr.get(1).concat(" " + bests.get(1)));
            top.setThird(bestStr.get(2).concat(" " + bests.get(2)));

            top.setDate(today.getTime());
            top.setIndex(index);
            topThree.save(top);

        }

        return top;
    }

    public TopThree topThreeIndexFood(String index) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {


        //DATA ODIERNA
        GregorianCalendar today = new GregorianCalendar();
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        today.add(Calendar.DAY_OF_MONTH, -1);

        TopThree ret = topThree.findTopThreeByDateAndIndex(today.getTime(), index);

        if (ret != null) {
            return ret;
        }

        MessageIndexCompute msg = new MessageIndexCompute();

        //Setting Stringhe descrittori di tutti i prodotti
        ArrayList<String> productDesc = new ArrayList<>();
        ArrayList<Foods> allProds = repositoryFood.findFoodsByLocation(GlobalVariable.RomaCentro);
        for (int i = 0; i < allProds.size(); i++) {
            productDesc.add(allProds.get(i).getNome());
        }
        msg.setDescProduct(productDesc);

        //Set time start
        GregorianCalendar start = new GregorianCalendar();
        GregorianCalendar end = new GregorianCalendar();
        start.set(Calendar.HOUR, 0);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);
        start.set(Calendar.MILLISECOND, 0);
        start.add(Calendar.MONTH, -1);
        start.add(Calendar.DAY_OF_MONTH, -2);
        msg.setStart(start.getTime());

        //Set time end
        end.set(Calendar.HOUR, 0);
        end.set(Calendar.MINUTE, 0);
        end.set(Calendar.SECOND, 0);
        end.set(Calendar.MILLISECOND, 0);
        msg.setEnd(end.getTime());

        //Set location
        ArrayList<String> location = new ArrayList<>();
        location.add(GlobalVariable.RomaCentro);
        msg.setLocation(location);
        msg.setGranularity(1);
        msg.setFlag("");

        //Set Index
        msg.setIndex(index);

        ArrayList<Result> result = serviceIndex.computeIndex(msg);
        ArrayList<Double> bests = new ArrayList<>();
        ArrayList<String> bestStr = new ArrayList<>();
        if (result.size() > 3) {
            int j = 0;
            while (j < 3) {
                double best = 0;
                String name = "";
                int indexMax = 0;
                for (int i = 0; i < result.size(); i++) {
                    //System.out.println("RESULT: " + result.get(0).getVar());
                    double var = result.get(i).getVar().get(0);
                    if (var >= best) {
                        best = var;
                        name = msg.getDescProduct().get(i);
                        indexMax = i;
                    }
                }
                System.err.println("Nome:" + name + " - Valore: " + best);
                j++;
                bestStr.add(name);
                bests.add(best);
                msg.getDescProduct().remove(indexMax);
                result.remove(indexMax);
            }
        }
        TopThree top = new TopThree();
        if (bests.size() == 3 && bestStr.size() == 3) {

            top.setFirst(bestStr.get(0).concat(" " + bests.get(0)));
            top.setSecond(bestStr.get(1).concat(" " + bests.get(1)));
            top.setThird(bestStr.get(2).concat(" " + bests.get(2)));
            top.setDate(today.getTime());
            top.setIndex(index);
            topThree.save(top);

        }

        return top;
    }


    public void deleteCache() {

        topThree.deleteAll();

    }

}
