package com.isssr_backend.parser;

import com.isssr_backend.variables.util.Result;

import java.util.ArrayList;

/**
 * Created by Fabrizio on 30/06/2017.
 */
public class LogicEngine {

    // applicazione dell'operazione elementare
    static private double compute(String op, double A, double B) {

        if (op.equals("+")) {
            return A + B;
        }
        if (op.equals("-")) {
            return A - B;
        }
        if (op.equals("*")) {
            return A * B;
        }
        if (op.equals("/")) {

            // zero division
            if(B == 0){
                return 0;
            }

            return A / B;
        }
        if (op.equals("^")) {
            return Math.pow(A, B);
        }


        System.err.println("Operatore sconosciuto : " + op + ". Arresto.");
        System.exit(-1);

        return 0;
    }


    // funzione che esegue un operazione
    static private void doOperation(int opType, double constA, double constB, LogicElement op, LogicElement a, LogicElement b) {

        // priorità del risultato
        int finalPriority = op.getPriority() - ParserIndex.getSumSubPriority();

        // simple op
        if (opType == 1) {

            // setta in a il risultato della somma
            a.setInfo(Double.toString(compute(op.getInfo(), constA, constB)));
        }

        // list and list
        if (opType == 2) {

            // per ogni resultProduct nella variabile
            for (int i = 0; i < a.getPartials().size(); i++) {

                // tmp referenct to A current resultProduct
                ArrayList<Double> aVar = a.getPartials().get(i).getVar();

                // tmp referenct to B current resultProduct
                ArrayList<Double> bVar = b.getPartials().get(i).getVar();

                // per la lunghezza di ogni singola lista
                for (int j = 0; j < aVar.size(); j++) {

                    // a[j] = a[j] + b[j]
                    aVar.set(j, compute(op.getInfo(), aVar.get(j), bVar.get(j)));
                }
            }
        }

        // constant and list
        if (opType == 3) {

            // per ogni resultProduct nella variabile
            for (int i = 0; i < b.getPartials().size(); i++) {

                // tmp referenct to A current resultProduct
                double aVal = new Double(a.getInfo());

                // tmp referenct to B current resultProduct
                ArrayList<Double> bVar = b.getPartials().get(i).getVar();

                // per la lunghezza di ogni singola lista
                for (int j = 0; j < bVar.size(); j++) {

                    // a[j] = a[j] + b[j]
                    bVar.set(j, compute(op.getInfo(), bVar.get(j), aVal));
                }
            }

            // ora A è una variable, non più costante
            a.setOggetto(b.getOggetto());
            a.setPartials(b.getPartials());
        }

        // list and constant
        if (opType == 4) {

            // per ogni resultProduct nella variabile
            for (int i = 0; i < a.getPartials().size(); i++) {

                // tmp referenct to A current resultProduct
                ArrayList<Double> aVar = a.getPartials().get(i).getVar();

                // tmp referenct to B current resultProduct
                double bVal = new Double(b.getInfo());

                // per la lunghezza di ogni singola lista
                for (int j = 0; j < aVar.size(); j++) {

                    // a[j] = a[j] + b[j]
                    aVar.set(j, compute(op.getInfo(), aVar.get(j), bVal));
                }
            }
        }

        // scala la priorità
        a.setPriority(finalPriority);
    }

    // funzione che esegue un operazione tra i dati a e b
    static private void computeOperation(LogicElement op, LogicElement a, LogicElement b) {

        /**
         * I casi possibili sono i seguenti:
         *
         * -  1  : operazione tra due costanti
         * -  2  : operazione tra due variabili
         * -  3  : operazione tra una costante e una variabile
         * -  4  : operazione tra una variabile e una costante
         * */

        double constA = 0;
        if (a.getOggetto().equals("constant")) {
            constA = new Double(a.getInfo());
        }

        double constB = 0;
        if (b.getOggetto().equals("constant")) {
            constB = new Double(b.getInfo());
        }

        // tipo operazione
        int opType;

        // sono 2 costanti
        if (a.getOggetto().equals("constant") && b.getOggetto().equals("constant")) {
            opType = 1;
        }
        // sono a costante e b variabile
        else if (a.getOggetto().equals("constant") && !b.getOggetto().equals("constant")) {
            opType = 3;
        }
        // sono a variabile e b costante
        else if (!a.getOggetto().equals("constant") && b.getOggetto().equals("constant")) {
            opType = 4;
        }
        // sono due variabili
        else {
            opType = 2;
        }

        // operazione
        doOperation(opType, constA, constB, op, a, b);
    }


    // funzione che restituisce la posizione dell'operatore a priorità più alta
    static private int getTopPriorityOperatorIndex(ArrayList<LogicElement> listaElem) {

        // operatore a top priority
        int topOperatorPosition = -1;

        //scansiono alla ricerca della priorità maggiore
        for (int i = 0; i < listaElem.size(); i++) {

            LogicElement e = listaElem.get(i);

            // lavoro solo sugli operatori
            if (e.getOggetto().equals("operator")) {

                // è il primo operatore incontrato
                if (topOperatorPosition == -1) {
                    topOperatorPosition = i;
                }
                // è un altro operatore, testo la priorità rispetto al vecchio top priority
                else {
                    // get top priority
                    LogicElement top = listaElem.get(topOperatorPosition);

                    // test della priorità
                    if (e.getPriority() > top.getPriority()) {

                        topOperatorPosition = i;
                    }
                }
            }
        }

        return topOperatorPosition;
    }


    // funzione che presa la lista di logic element ne computa il valore finale
    static public ArrayList<Result> operateList(ArrayList<LogicElement> listaElem) {

        // indice del prossimo operatore da fare
        int nextOperatorIndex;

        // cerca l'operatore a priorità più alta
        while (listaElem.size() != 1) {

            // ottieni il prossimo miglior operatore
            nextOperatorIndex = getTopPriorityOperatorIndex(listaElem);

            // applica l'operatore
            computeOperation(listaElem.get(nextOperatorIndex), listaElem.get(nextOperatorIndex - 1), listaElem.get(nextOperatorIndex + 1));

            //  a non viene rimossa perchè si riusa come store del risultato

            // remove op
            listaElem.remove(nextOperatorIndex);

            // remove b
            listaElem.remove(nextOperatorIndex);
        }


        // final value is not a list, is a constant
        if (listaElem.get(0).getPartials() == null) {
            insertRowNumber(listaElem.get(0));
        }

        return listaElem.get(0).getPartials();
    }


    // funzione che setta un numero puro come risultato
    private static void insertRowNumber(LogicElement elem) {

        // generate new resultProduct list
        ArrayList<Result> res = new ArrayList<Result>();

        // populate it
        ArrayList<Double> fakeList = new ArrayList<Double>();

        // store info number
        fakeList.add(new Double(elem.getInfo()));

        Result r = new Result(fakeList);

        // add fake resultProduct to the list
        res.add(r);

        // store final resultProduct
        elem.setPartials(res);
    }
}
