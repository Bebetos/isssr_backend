package com.isssr_backend.parser.CYK;

import com.isssr_backend.variables.util.ClassFinder;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by emanuele on 05/07/17.
 */
public class ParserCYK {

    private static boolean debug = false;

    private static String path =
            "src" + File.separator +
                    "main" + File.separator +
                    "java" + File.separator +
                    "com" + File.separator +
                    "isssr_backend" + File.separator +
                    "parser" + File.separator +
                    "CYK" + File.separator;

    private static String baseGrammar = "" +
            "\t% questa flag è richiesta dal programma per identificare la produzione con l'assioma\n" +
            "\t__AXIOM__ = ⛸\n" +
            "\n" +
            "\t% il simbolo '⛸ ' è l'assioma della grammatica, ed ammette un'espressione di base come \n" +
            "\t% simbolo di partenza, tale espressione di base può essere preceduta dal segno meno.\n" +
            "\t⛸ -> ⛽\n" +
            "\n" +
            "\t% il simbolo '⛽' ammette chiusura tra parentesi di ampiezza arbitraria.\n" +
            "\t⛽ -> (⛽) \n" +
            "\n" +
            "\t% il simbolo '⛽' ammette combinazioni con tutti gli operatori matematici di base.\n" +
            "\t⛽ -> ⛽*⛽ | ⛽+⛽ | ⛽-⛽ | ⛽/⛽ | ⛽^⛽\n" +
            "\t\n" +
            "\t% il simbolo '⛽' espande in un numero intero, un numero reale o una funzione.\n" +
            "\t⛽ -> ♻ | ♻.♻ | ⛿\n" +
            "\n" +
            "\t% il simbolo '♻' espande in un numero intero arbitrariamente grande.\n" +
            "\t♻ -> ⛔♻ | ⛔\n" +
            "\t⛔ -> 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9";

    // parser entry point
    public static boolean parse(String semantic, String text) {

        try {

            ArrayList<String> variablesNames = ClassFinder.getVariables(semantic);

            // get fullgrammar content
            String grammar = buildMissingLines(semantic, variablesNames);

            // save it
            storeGrammar(grammar);

            // start parser
            return pythonBridge(text);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return false;
    }

    // debug call
    public static void test(String s){
        System.err.println(parse("product",s));
    }

    // print grammar to file
    private static void storeGrammar(String grammar) throws FileNotFoundException, UnsupportedEncodingException {
        PrintWriter writer = new PrintWriter(path + "grammar.txt", "UTF-8");
        writer.print(grammar);
        writer.close();
    }

    // questa funzione aggiorna in modo automatico la grammatica perchè accetti le variabili custom
    private static String buildMissingLines(String semantic, ArrayList<String> variables) {

        String appender = "\t%il simbolo '⛿' espande nelle variabili riconosciute.\n";
        for (String var : variables) {
            appender += "\t⛿ -> " + semantic + "." + var + "\n";
        }

        return baseGrammar + appender;
    }


    // funzione usata per lanciare un CYK parser scritto in python
    private static boolean pythonBridge(String text) throws IOException, InterruptedException {

        int exitValue = launchAndWatch(text, debug, debug);

        if (exitValue != 1 && exitValue != 0) {
            System.err.println("Invalid python parser state : " + exitValue);
        }

        if (exitValue == 1) {
            return true;
        } else {
            return false;
        }
    }


    // launcher
    private static int launchAndWatch(String text, Boolean stdout, Boolean stderr) throws IOException, InterruptedException {
        ProcessBuilder probuilder;

        String s = "";

        // remove white spaces
        text = text.replaceAll(" ", "");

        // safe input reading
        text = '"' + text + '"';

        // windows call
        if (System.getProperty("os.name").toLowerCase().contains("windows")) {
            probuilder = new ProcessBuilder("cmd.exe", "/c", "python", "CYK.py", text);
        }
        // linux call
        else {
            probuilder = new ProcessBuilder("python3", "CYK.py", text);
        }

        //You can set up your work directory
        probuilder.directory(new File(path));

        // launch
        Process process = probuilder.start();

        // allow stdout
        if (stdout) {
            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(process.getInputStream()));

            // read the output from the command
            System.out.println("Here is the standard output of the command:\n");
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
            }
        }

        // allow stderr
        if (stderr) {
            BufferedReader stdError = new BufferedReader(new
                    InputStreamReader(process.getErrorStream()));

            // read any errors from the attempted command
            System.out.println("Here is the standard error of the command (if any):\n");
            while ((s = stdError.readLine()) != null) {
                System.out.println(s);
            }
        }

        return process.waitFor();
    }
}
