# ISSSR Backend Repository #

This is a Spring MVC application for the exam of "Ingegneria dei Sistemi Software e dei Servizi di Rete".<br>
The application's goal is to give to the user the possibility to do data analysis for a food restaurants and bars chain.<br>
This tool give the possibility to an expert user to create custom index, calculate its value along a period and save in a .pdf report.

# Getting Started ##
### Prerequisites ###

* Install python for the execution of the parser
* Install the dependacies using maven
* Create your own MongoDB server (update the information into application.properties) or use our

### Running ###

The backend is listening on port 8012

### Utilities ###

If you are creating your own JSON for put the data from json-generator.com and you have problem with the date fields feel free to use my python script for adjust the json.<br>
```
 git clone https://Bebetos92@bitbucket.org/Bebetos92/scriptpython.git
```

### Team ###

* Alberto Talone
* Andrea Cenciarelli
* Giorgio Marini
* Emanuele Serrao
* Fabio Di Giacomo
* Fabrizio Guglietti